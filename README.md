# Building Blocks

This module provides several basic but versatile block types. It is dependent on Shea Dawson's SilverStripe Blocks module. As you may be aware, content blocks provide a number of benefits over a single content field:

* Content may be separated into distinct sections, which is especially useful for pages with longer content.
* Content blocks allow additional features not provided by a single HTML editor field.
* Content blocks may provide more advanced layout options, based on pre-defined templates.
* Individual content blocks may be unpublished if necessary, rather than the entire page.
* Content blocks may be used on multiple pages, without duplicating content.

## Installation
The module may be installed with composer:

    composer require "tkisilverstripeteam/tkibuildingblocks:*"

## Available block types

* * *
### Text / Media Block

The "Text / Media" block is useful for creating content composed of text and/or images, with a variety of different layouts.

To create a new block, select the "Text / Media" block type from the drop down list, then click "Add". The form for creating the content has five tabs entitled: "Main", "Appearance", "Behaviour", "Media", and "Viewer Groups".

##### Main

Fill in the main fields for the the block. Select the block area (usually "Main Content"), enter a title, and fill in the content. The title is used to identify the block in the CMS, and is displayed as a heading for the block on the web page. Select the hierarchical heading level from the "Heading type" drop down menu. To hide the heading on the web page, select "Hidden". If a subtitle is also required, you may enter it in the "Subtitle" field.

![Figure B.2](docs/images/textmedia-main.png)
Figure B.2

##### Appearance

Several layout options are available for selection. Choose an appropriate layout from the pre-defined layout options.

![Figure B.3](docs/images/textmedia-appearance.png)
Figure B.3

##### Behaviour

If the block will contain images / media, next decide how the media should be displayed by selecting a mode from the options on the "Behaviour" tab.

![Figure B.4](docs/images/textmedia-behaviour.png)
Figure B.4

##### Media

If you are creating a new block, you will first need to save the block before adding images. Press the "Save draft" button at the bottom of the screen. Next, click the "Add Media Item" button, and the media item form will appear.

![Figure B.5](docs/images/textmedia-media-add.png)
Figure B.5

The media item form allows you to upload a new image or select a file from the CMS "Files" section. If uploading from your computer, you may either drag-and-drop the file or click the "From your computer" button to open a file selection window. The target upload folder may also be changed if needed, by clicking the target folder link.

![Figure B.6](docs/images/textmedia-media-main.png)
Figure B.6

If desired, fill out other optional details such as image link, caption, and attribution information. Then click the "Save and close" button.

##### Viewer Groups (optional)

If the content block is not meant to be publicly viewable when published, you may restrict who may view the block using the options provided. For example, for member-only content, you could restrict the display of the block to certain user groups, such as "Subscribers".

* * *
### Links block
This block type is useful for creating a list of links to pages, files, or external URLs.

To create a new block, select the "Links" block type from the drop down list, then click "Add". The form for creating the content has four tabs entitled: "Main", "Appearance"," Links", and "Viewer Groups".

##### Main
Fill in the main fields for the the block. Select the block area (usually "Main Content") and enter a title. Select the hierarchical heading level from the "Heading type" drop down menu. To hide the heading on the web page, select "Hidden". If a subtitle is also required, you may enter it in the "Subtitle" field.

##### Appearance

There are three layouts for presenting the links:

* *Icon list* - A simple list displaying the title and description. Icons denote the file type.
* *Thumbnail list* - A list of links displaying the title and description, with thumbnail images for each link. Also known as media objects. Thumbnails must be uploaded/selected for each link entered.
* *Thumbnail grid* - A grid of thumbnail images, displaying a thumbnail and title for each link. Thumbnails must be uploaded/selected for each link entered.

Choose an appropriate layout from the pre-defined layout options.

![Figure B.8](docs/images/links-appearance.png)
Figure B.8

##### Links
If you are creating a new block, you will first need to save the block before adding links. Press the "Save draft" button at the bottom of the screen. Next, click the "Add Link" button, and the link form will appear.

![Figure B.9](docs/images/links-links-main.png)
Figure B.9

If you are using a layout requiring thumbnails, click on the "Thumbnail" tab, then upload or select an existing image to be used for the thumbnail. The thumbnail will be resized and cropped automatically to the configured size.

![Figure B.10](docs/images/links-links-thumb.png)
Figure B.10

##### Viewer Groups (optional)
If the content block is not meant to be publicly viewable when published, you may restrict who may view the block using the options provided. For example, for member-only content, you could restrict the display of the block to certain user groups, such as "Subscribers".

* * *
### IFrame block
To include an IFrame in your content, select the "IFrame" block type from the drop down list, then click "Add". Fill in the standard fields, then enter:

* the URL for the IFrame,
* the width of the IFrame (typically 100% for responsiveness), and
* the height of the IFrame (typically in pixels).

![Figure B.11](docs/images/iframeblock-edit.png)
Figure B.11

* * *
### HTML block
To include plain HTML in your content, select the "HTML" block type from the drop down list, then click "Add". Fill in the standard fields, then enter the HTML into the content field. (Note that this block may be restricted by permissions.)

![Figure B.12](docs/images/htmlblock-edit.png)
Figure B.12
