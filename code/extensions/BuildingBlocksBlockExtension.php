<?php


class BuildingBlocksBlockExtension extends DataExtension
{

	private static $db = array(
		'Description' => 'Text',
		'TitleTag' => "Enum('h2,h3,h4,h5,h6,hidden','h2')",
		'SubTitle' => "Varchar(255)",
        'FxType' => "Varchar(40)",
        //'FxDefaultState' => "Varchar(40)"
	);

    
    public function updateCMSFields(\FieldList $fields) {
        $fields->removeByName('TitleTag');
		$fields->removeByName('SubTitle');
        $fields->removeByName('FxType');
        //$fields->removeByName('FxDefaultState');
        
		$titleTagOptions = $this->owner->dbObject('TitleTag')->enumValues();
        
		foreach($titleTagOptions as $k => $v) {
			$titleTagOptions[$k] = _t('BuildingBlocksBlockExtension.TitleTag-'. $k, $k);
		}
		$fields->addFieldToTab('Root.Main',DropdownField::create('TitleTag',_t('BuildingBlocksBlockExtension.TitleTag','Heading level'),$titleTagOptions),'Description');
		$fields->addFieldToTab('Root.Main',TextField::create('SubTitle',_t('BuildingBlocksBlockExtension.SubTitle','Subtitle')),'Description');
        
        $fields->replaceField('Description', TextareaField::create('Description',_t('BuildingBlocksBlockExtension.Description','Description (back end)')));
        
        // FX
        $fxTypes = Config::inst()->get(get_class($this->owner),'fx_types',Config::UNINHERITED);

        if(count($fxTypes)) {
            // Fx Options
            $fxTypeOptions = [];
            foreach($fxTypes as $fxType) {
                $fxTypeOptions[$fxType] = _t('BuildingBlocksBlockExtension.FxType-'. Convert::raw2att($fxType), ucfirst($fxType));
            }

            $fields->findOrMakeTab('Root.Effects',_t('BuildingBlocksBlockExtension.FxTab','Effects'));
            $fxTypeField = DropdownField::create('FxType',_t('BuildingBlocksBlockExtension.FxType','Effect type'),$fxTypeOptions)
                ->setHasEmptyDefault(true);
            $fields->addFieldToTab('Root.Effects',$fxTypeField);
            /** @todo
            $fxStates = ['active','inactive'];
            $fxStateOptions = [];
            foreach($fxStates as $fxState) {
                $fxStateOptions[$fxState] = _t('BuildingBlocksBlockExtension.FxState-'. Convert::raw2att($fxState), ucfirst($fxState));
            }
            $fxStateField = DropdownField::create('FxDefaultState',_t('BuildingBlocksBlockExtension.FxDefaultState','Default State'),$fxStateOptions)
                ->setHasEmptyDefault(true);
            $fields->addFieldToTab('Root.Effects',$fxStateField);
             * 
             */
        }
 
            
    }
 
    public function FxTypeClasses()
	{
        if(empty($this->owner->FxType)) {
            return '';
        }
        
		$class = 'bb-fx__'. Convert::raw2att($this->owner->FxType);
        /*if(!empty($this->owner->FxDefaultState)) {
            $class .= $class .'--'. Convert::raw2att($this->owner->FxDefaultState);
        }*/
        
        return $class;
	}
    
    public function FxTitleClass()
	{
        if(empty($this->owner->FxType)) {
            return '';
        }
        $type = Convert::raw2att($this->owner->FxType);
		return 'bb-fx__'. $type .'-title';
	}
    
}
