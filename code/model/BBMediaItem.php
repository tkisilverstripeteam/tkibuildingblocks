<?php


class BBMediaItem extends DataObject implements PermissionProvider
{
	private static $db = array(
		'Type' => "Enum('InternalImage,ExternalMedia','InternalImage')",
		'Title' => 'Varchar(255)',
        'Alt' => 'Varchar(255)',
        /** @todo Embeddable
		 'SourceURL' => 'Varchar(255)',
         'Width' => 'Varchar',
        'Height' => 'Varchar',
        'ThumbURL' => 'Varchar(255)',
        'ExtraClass' => 'Varchar(64)',
        'EmbedHTML' => 'Text',*/
		'Description' => 'HTMLText',
        'LinkType' => "Enum('internal-page,internal-file,external','')",
		'LinkURL' => 'Text',
		'LinkTarget' => "Enum('_self,_blank','_self')",
		'Credit' => 'Varchar',
		'CreditURL' => 'Varchar',
		'License' => 'Varchar',
		'LicenseURL' => 'Varchar',
		'Sort' => 'Int',
        'Disabled' => 'Boolean',
        'NoBorder' => 'Boolean'
	);
    
	private static $has_one = array(
		'InternalImage' => 'Image',
		'Block' => 'BlockTextMedia',
		'LinkPage' => 'SiteTree',
        'LinkFile' => 'File'
	);

	private static $summary_fields = array(
		'ID' => 'ID',
		'InternalImage.StripThumbnail' => 'Image',
		'Title' => 'Title',
		'Type' => 'Type',
        'Disabled' => 'Disabled'
	);

	private static $searchable_fields = array(
		'ID',
		'Title',
		'Description'
	);

	protected $link;

	public function singular_name()
	{

		return _t('BBMediaItem.SINGULARNAME', 'Media Item');
	}

	public function plural_name()
	{
		return _t('BBMediaItem.PLURALNAME', 'Media Items');
	}

	public function getAllCMSActions()
	{
		$actions = new FieldList();
		$actions->push(
			FormAction::create('save',_t('CMSMain.SAVE','Save'))
				->addExtraClass('ss-ui-action-constructive')->setAttribute('data-icon', 'accept')
		);
		return $actions;
	}
	public function getCMSFields()
	{
		/* 
		 * Main tab
		 */
		$fields = $this->scaffoldFormFields(array(
			'includeRelations' => false,
			'tabbed' => true,
			'ajaxSafe' => true,
			'restrictFields' => array('InternalImage')
		));
		
		// Default uploads folder
		$siteConfig = SiteConfig::current_site_config();
		if($siteConfig->hasExtension('TkiThemeConfigSiteConfigExtension')) {
			$siteConfig->initDefaultUploadsFolder();
		}
		
        /*
         * Image
         */
        $fields->addFieldToTab('Root.Main',HeaderField::create('ImageHeading',_t('BBMediaItem.ImageHeading', 'Image'),3),'InternalImage');
        
		// Upload field
		$uploadField = UploadField::create('InternalImage',_t('BBMediaItem.SelectImage', 'Select / upload image'));
		$uploadField->setAllowedFileCategories('image');

		$fields->replaceField('InternalImage',$uploadField);
      
		$fields->addFieldsToTab('Root.Main',array(
            TextField::create('Alt',_t('BBMediaItem.Alt','Alternative text')),
		));

        $fields->addFieldsToTab('Root.Main',array(FieldGroup::create(
            CheckboxField::create('NoBorder',_t('BBMediaItem.NoBorder','No border')),
            CheckboxField::create('Disabled',_t('BBMediaItem.Disabled','Disabled'))
        )));
            
        /*
         * Link
         */
        $fields->addFieldToTab('Root.Main',HeaderField::create('LinkHeading',_t('BBMediaItem.LinkHeading', 'Link'),3));
        
        // Link type options
		$linkTypeOptions = $this->dbObject('LinkType')->enumValues();
		foreach($linkTypeOptions as $k => $v) {
			$linkTypeOptions[$k] = _t('BBMediaItem.LinkType-'.$k);
		}
        
        $linkTypeField = OptionsetField::create('LinkType',
            _t('BBMediaItem.LinkType','Link type'),
            $linkTypeOptions
        );
        $fields->addFieldToTab('Root.Main',$linkTypeField);
        
        // Page tree field
		$treeField = DisplayLogicWrapper::create(TreeDropdownField::create('LinkPageID',_t('BBMediaItem.SelectLinkPage','Link to page'),'SiteTree'));
		$fields->addFieldToTab('Root.Main',$treeField);

        // File upload field
		$fileField = UploadField::create('LinkFile',_t('BBMediaItem.SelectLinkFile', 'Select file'));
		$fileField->setAllowedFileCategories(array('audio','mov','zip','image','doc'));
		$linkFileField = DisplayLogicWrapper::create($fileField);
		$fields->addFieldToTab('Root.Main',$linkFileField);

        $urlField = TextField::create('LinkURL',_t('BBMediaItem.LinkURL','External link URL'));
        $fields->addFieldToTab('Root.Main',$urlField);
        
        // Target options
		$targetOptions = $this->dbObject('LinkTarget')->enumValues();
		foreach($targetOptions as $k => $v) {
			$targetOptions[$k] = _t('BBMediaItem.LinkTarget'. $k);
		}
        $targetField = OptionsetField::create('LinkTarget',_t('BBMediaItem.LinkTarget','Target window'),$targetOptions);
        
        $fields->addFieldToTab('Root.Main',$targetField);
        
		// Display logic
		$treeField->displayIf('LinkType')->isEqualTo('internal-page');
		$linkFileField->displayIf('LinkType')->isEqualTo('internal-file');
		$urlField->displayIf('LinkType')->isEqualTo('external');
        
		/*
		 * Caption tab
		 */
		$fields->findOrMakeTab('Root.Caption',_t('BBMediaItem.CaptionTab','Caption'));
		$fields->addFieldsToTab('Root.Caption',array(
			TextField::create('Title',_t('BBMediaItem.Title', 'Title')),
			HTMLEditorField::create('Description',_t('BBMediaItem.Description', 'Description'))
		));

		/*
		 * Attribution tab
		 */
		$fields->findOrMakeTab('Root.Attribution',_t('BBMediaItem.AttributionTab','Attribution'));
		$fields->addFieldsToTab('Root.Attribution',array(
			TextField::create('Credit',_t('BBMediaItem.Credit')),
			TextField::create('CreditURL',_t('BBMediaItem.CreditURL')),
			TextField::create('License',_t('BBMediaItem.License')),
			TextField::create('LicenseURL',_t('BBMediaItem.LicenseURL'))
		));

		// Extension hook
		$this->extend('updateCMSFields', $fields);
		return $fields;
	}

 	protected function setLinkURL($value)
	{
		$this->setField('LinkURL',$this->prepareURL($value));
	}

	protected function setCreditURL($value)
	{
		$this->setField('CreditURL',$this->prepareURL($value));
	}

	protected function setLicenseURL($value)
	{
		$this->setField('LicenseURL',$this->prepareURL($value));
	}

	/**
	 * Adds scheme if missing
	 * @param  string $value
	 * @return string
	 */
	protected function prepareURL($value)
	{
		$scheme = parse_url($value,PHP_URL_SCHEME);
		if(!empty($value) && (empty($scheme) || strpos($value,'://' === false))) {
			$value = 'http://'. $value;
		}
		return $value;
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
                'Alt' => _t('BBMediaItem.Alt', 'Alternative text'),
				'Type' => _t('BBMediaItem.Type', 'Type'),
				'Credit' => _t('BBMediaItem.Credit', 'Credit'),
				'CreditURL' => _t('BBMediaItem.CreditURL', 'Credit URL'),
				'License' => _t('BBMediaItem.License', 'License'),
				'LicenseURL' => _t('BBMediaItem.LicenseURL', 'License URL'),
                'NoBorder' => _t('BBMediaItem.NoBorder', 'No border'),
                'Disabled' => _t('BBMediaItem.Disabled', 'Disabled')
			)
		);
	}

	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
	public function MediaLink()
	{
        switch($this->LinkType) {
            case 'internal-page':
                return $this->linkToInternalPage();
                break;
            case 'internal-file':
                return $this->linkToInternalFile();
                break;
            case 'external':
                return $this->linkToExternalUrl();
                break;
            default:
                // No link type specified (legacy)
                if($this->LinkPageID) {
                    return $this->linkToInternalPage();
                } elseif($this->LinkFileID) {
                    return $this->linkToInternalFile();
                } elseif(!empty($this->LinkURL)) {
                    return $this->linkToExternalUrl();
                } else {
                    return '';
                }
                break;
        }
	}
    
    public function HasCaption()
	{
        $caption = trim($this->Title) . trim($this->Description) . trim($this->Credit) . trim($this->License);
        return strlen($caption);
    }

    protected function linkToInternalPage()
    {
        $page = $this->LinkPage();
		return ($page && !empty($page->ID)) ? $page->AbsoluteLink() : '';
    }
    
    protected function linkToInternalFile()
    {
        $file = $this->LinkFile();
		return ($file && !empty($file->ID)) ? $file->Link() : '';
    }
    
    protected function linkToExternalUrl()
    {
        return $this->LinkURL;
    }
    
	/*
	 * -------------------------------------------------------------------------
	 * Permission methods
	 * -------------------------------------------------------------------------
	 */
	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		// Inherit block permission if linked
		$block = $this->Block();
		if($block) {
			return $block->canView($member);
		}
		// Publicly viewable if not restricted specified on block
		return true;
	}

	/**
	 * @todo Should canCreate be a static method?
	 *
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}




	public function providePermissions()
    {
        return array(
			'BBMEDIAITEM_MANAGE' => array(
                'name' => _t(
                    'BBMediaItem.ManagePermission',
                    'Manage media items'
                ),
                'category' => _t(
                    'BBMediaItem.PermissionCategory',
                    'Media Items'
                ),
            )
        );
    }


}
