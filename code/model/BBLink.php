<?php


class BBLink extends DataObject
{
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Description' => 'Text',
		'Source' => "Enum('internal-page,internal-file,external')",
		'Type' => "Enum('audio,mov,zip,image,doc,pdf,spreadsheet,webpage,none','none')",
		'ExternalURL' => 'Text',
		'Target' => "Enum('_self,_blank','_self')",
        'Disabled' => 'Boolean',
        'ActionText' => 'Varchar(50)',
		'Sort' => 'Int'
	);

	private static $has_one = array(
		'InternalPage' => 'SiteTree',
		'InternalFile' => 'File',
		'Thumbnail' => 'Image',
		'Block' => 'BlockLinks'
	);

	private static $summary_fields = array(
		'ID' => 'ID',
		'Title' => 'Title',
		'Source' => 'Source',
		'Type' => 'Type',
        'DisabledStatus' => 'DisabledStatus',
        'Thumbnail.StripThumbnail' => 'Thumbnail'
	);

	private static $searchable_fields = array(
		'ID',
		'Title',
		'Description'
	);
	
	private static $casting = array(
		'Link' => 'Text',
        'DisabledStatus' => 'HTMLText',
	);

	/**
	 * Modified version of File::app_categories
	 * @config
	 * @var array Category identifiers mapped to commonly used extensions.
	 */
	private static $link_categories = array(
		'audio' => array(
			"aif" ,"au" ,"mid" ,"midi" ,"mp3" ,"ra" ,"ram" ,"rm","mp3" ,"wav" ,"m4a" ,"snd" ,"aifc" ,"aiff" ,"wma",
			"apl", "avr" ,"cda" ,"ogg"
		),
		'mov' => array(
			"mpeg" ,"mpg" ,"mp4" ,"m1v" ,"mp2" ,"mpa" ,"mpe" ,"ifo" ,"vob","avi" ,"wmv" ,"asf" ,"m2v" ,"qt", "ogv", "webm"
		),
		'zip' => array(
			"arc" ,"rar" ,"tar" ,"gz" ,"tgz" ,"bz2" ,"dmg" ,"jar","ace" ,"arj" ,"bz" ,"cab"
		),
		'image' => array(
			"bmp" ,"gif" ,"jpg" ,"jpeg" ,"pcx" ,"tif" ,"png" ,"alpha","als" ,"cel" ,"icon" ,"ico" ,"ps"
		),
		'flash' => array(
			'swf', 'fla'
		),
		'doc' => array(
			'doc','docx','txt','rtf','pages', 'ppt','pptx','pps'
		),
		'spreadsheet' => array(
			'xls','xlsx','csv'
		),
		'webpage' => array(
			'html','htm','xhtml','xml'
		),
		'pdf' => array(
			'pdf'
		)
	);
	
	public function singular_name()
	{

		return _t('BBLink.SINGULARNAME', 'document');
	}

	public function plural_name()
	{
		return _t('BBLink.PLURALNAME', 'documents');
	}

	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */
	
	public function getCMSFields()
	{
		/*
		 * Main tab
		 */
		$fields = $this->scaffoldFormFields(array(
			'includeRelations' => false,
			'tabbed' => true,
			'ajaxSafe' => true,
			'restrictFields' => array('Title','Description','ActionText')
		));
		
		// Source options
		$sourceOptions = $this->dbObject('Source')->enumValues();
		foreach($sourceOptions as $k => $v) {
			$sourceOptions[$k] = _t('BBLink.Source-'.$k,$k);
		}
		$sourceField = OptionsetField::create('Source',_t('BBLink.Source','Source'),$sourceOptions);
		$fields->addFieldsToTab('Root.Main',$sourceField);
		
		// Default uploads folder
		$siteConfig = SiteConfig::current_site_config();
		if($siteConfig->hasExtension('TkiThemeConfigSiteConfigExtension')) {
			$siteConfig->initDefaultUploadsFolder();
		}
		// Page tree field
		$treeField = DisplayLogicWrapper::create(TreeDropdownField::create('InternalPageID',_t('BBLink.Page','Page'),'SiteTree'));
		$fields->addFieldToTab('Root.Main',$treeField);
		
		// File upload field
		$fileField = UploadField::create('InternalFile',_t('BBLink.SelectFile', 'Select file'));
		$fileField->setAllowedFileCategories(array('audio','mov','zip','image','doc'));
		$uploadField = DisplayLogicWrapper::create($fileField);
		$fields->addFieldToTab('Root.Main',$uploadField);
		
		// External URL
		$urlField = TextField::create('ExternalURL',_t('BBLink.ExternalURL','External file URL'));
		$fields->addFieldToTab('Root.Main',$urlField);
			
		// Target
		$targetOptions = $this->dbObject('Target')->enumValues();
		foreach($targetOptions as $k => $v) {
			$targetOptions[$k] = _t('BBLink.Target'. $k);
		}
		$fields->addFieldToTab('Root.Main',OptionsetField::create('Target',_t('BBLink.Target','Target window'),$targetOptions));
		
		// Disabled
		$fields->addFieldToTab('Root.Main',CheckboxField::create('Disabled',_t('BBLink.Disabled','Disabled')));
		
		/*
		 * Thumbnail tab
		 */
		$fields->findOrMakeTab('Root.ThumbnailTab',_t('BBMediaItem.ThumbnailTab','Thumbnail'));
		$thumbField = UploadField::create('Thumbnail',_t('BBLink.SelectThumbnail', 'Select thumbnail image'));
		$thumbField->setAllowedFileCategories('image');
		$fields->addFieldToTab('Root.ThumbnailTab',$thumbField);

		// Display logic
		$treeField->displayIf('Source')->isEqualTo('internal-page');
		$uploadField->displayIf('Source')->isEqualTo('internal-file');
		$urlField->displayIf('Source')->isEqualTo('external');
		
		// Extension hook
		$this->extend('updateCMSFields', $fields);
		return $fields;
	}

	public function getCMSValidator() {
		return RequiredFields::create('Title','Source','Target');
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
				'Title' => _t('BBLink.Title', 'Title'),
				'Description' => _t('BBLink.Description', 'Description'),
                'ActionText' => _t('BBLink.ActionText', 'Call to action text'),
				'Source' => _t('BBLink.Source', 'Source'),
				'Type' => _t('BBLink.Type', 'File type'),
				'ExternalURL' => _t('BBLink.ExternalURL', 'External file URL'),
				'Target' => _t('BBLink.Target', 'Target window'),
                'Disabled' => _t('BBLink.Disabled', 'Disabled')
			)
		);
	}
	
	protected function setExternalURL($value)
	{
		$this->setField('ExternalURL',$this->prepareURL($value));
	}

	/**
	 * Adds scheme if missing
	 * @param  string $value
	 * @return string
	 */
	protected function prepareURL($value)
	{
		$scheme = parse_url($value,PHP_URL_SCHEME);
		if(!empty($value) && (empty($scheme) || strpos($value,'://' === false))) {
			$value = 'http://'. $value;
		}
		return $value;
	}

	
	/*
	 * -------------------------------------------------------------------------
	 * Event methods
	 * -------------------------------------------------------------------------
	 */
	public function onBeforeWrite()
	{
		parent::onBeforeWrite();
		
		if(!$this->exists || $this->isChanged()) {
			$this->Type = $this->determineFileType();
		}
	}
	
	protected function determineFileType()
	{
		$extension = null;
		switch($this->Source) {
			case 'internal-page':
				$extension = 'html';
				break;
			case 'internal-file':
				$file = $this->InternalFile();
				$filename = ($file) ? $file->filename : '';
				$extension = File::get_file_extension($filename);
				break;
			case 'external':
				$extension = File::get_file_extension($this->ExternalURL) ?: 'html';
				break;
		}
	
		return ($extension) ? static::get_link_category($extension) : 'none';
	}
	
	
	public function get_link_category($ext) {
		$ext = strtolower($ext);
		foreach(Config::inst()->get('BBLink', 'link_categories') as $cat => $exts) {
			if(in_array($ext, $exts)) return $cat;
		}
		return 'none';
	}
	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
    public function getFormattedSource()
    {
        return ($this->Source) ? _t('BBLink.Source-'.$this->Source,$this->Source) : '';
    }
    
    public function getFormattedDisabled()
    {
        return $this->obj('Disabled')->Nice();
    }
    
    public function DisabledStatus()
    {
        $status = $this->Disabled ? 'disabled' : 'enabled';
        
        $class = $this->Disabled ? 'danger' : 'success';
        $translated = _t('BBLink.DisabledStatus_'.$status,$status);
        
        $html = sprintf(
            "<span class=\"badge %s\" title=\"%s\">%s</span>",
            'badge-' . Convert::raw2xml($class),
            Convert::raw2xml($translated),
            Convert::raw2xml($translated)
        );

        return DBField::create_field('HTMLText',$html);
    }
    
	public function Link()
	{
		switch($this->Source) {
			case 'internal-page':
				$obj = $this->InternalPage();
				return ($obj) ? $obj->AbsoluteLink() : '';
				break;
			case 'internal-file':
				$obj = $this->InternalFile();
				return ($obj) ? $obj->AbsoluteLink() : '';
				break;
			case 'external':
				return $this->ExternalURL;
				break;
		}

	}

	/*
	 * -------------------------------------------------------------------------
	 * Permission methods
	 * -------------------------------------------------------------------------
	 */
	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		// Inherit block permission if linked
		$block = $this->Block();
		if($block) {
			return $block->canView($member);
		}
		// Publicly viewable if not restricted specified on block
		return true;
	}

	/**
	 * @todo Should canCreate be a static method?
	 *
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('ADMIN') || Permission::check('BBMEDIAITEM_MANAGE');
	}


}
