<?php


class BlockIFrame extends BlockBase {

	private static $db = array(
        'IFrameID' => 'Varchar',
		'URL' => 'Text',
		'Width' => 'Varchar',	// Pixels or percent
		'Height' => 'Varchar',	// Pixels or percent
		'AllowFullScreen' => 'Boolean'
	);

	/**
	 * @config
	 * @var array
	 */
	private static $defaults = array(
		'Width' => '100%',
		'Height' => '500px',
		'AllowFullScreen' => true
	);
	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	public function singular_name()
	{
		return _t("$this->class.SINGULARNAME", 'IFrame Block');
	}

	public function plural_name()
	{
		return _t("$this->class.PLURALNAME", 'IFrame Blocks');
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
                'IframeID' => _t("$this->class.IframeID", 'IFrame ID'),
				'URL' => _t("$this->class.URL", 'IFrame URL'),
				'Width' => _t("$this->class.Width", 'Width'),
				'Height' => _t("$this->class.Height", 'Height'),
				'AllowFullScreen' => _t("$this->class.AllowFullScreen", 'Allow full screen'),
			)
		);
	}

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		
        $fields->replaceField('IFrameID',
			TextField::create('IFrameID',_t('BlockIFrame.IFrameID','IFrame ID'))
		);
        
		$fields->replaceField('URL',
			TextField::create('URL',_t('BlockIFrame.URL','IFrame URL'))
		);
		
		$fields->replaceField('Width',
			TextField::create('Width',_t('BlockIFrame.Width','Width'))
				->setRightTitle(_t('BlockIFrame.WidthDescription','Optional: width in pixels or percent. (eg. 100%)'))
		);
		
		$fields->replaceField('Height',
			TextField::create('Height',_t('BlockIFrame.Height','Height'))
				->setRightTitle(_t('BlockIFrame.HeightDescription','Optional: height in pixels or percent. (eg. 500px)'))
		);
		
		return $fields;
	}

	protected function getCMSRequiredFields()
	{
		return array_unique(array_merge(parent::getCMSRequiredFields(),array('URL')));
	}
	
	/**
     * 
	 * Checks and sanitizes URL
     * @throws ValidationException
     * @return ValidationResult
     */
    public function validate()
    {
        $result = parent::validate();

        $schemes = array('http', 'https');
		$matches = parse_url($this->URL);

        if(!is_array($matches) || (isset($matches['scheme']) && !in_array($matches['scheme'], $schemes))) {
            $result->error(_t('BlockIFrame.InvalidURL', 'Invalid URL'));
        }

        return $result;
    }

  /*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */

  /**
	 * Gets specified width or configured default
	* @return string
	*/
	public function Width()
	{
		if(!empty($this->Width)) {
			return $this->Width;
		} else {
			$defaults = Config::inst()->get($this->class, 'defaults', Config::UNINHERITED);
			return isset($defaults['Width']) ? $defaults['Width'] : self::$defaults['Width'];
		}
	}

	/**
	 * Gets specified height or configured default
	* @return string
	*/
	public function Height()
	{
		if(!empty($this->Height)) {
			return $this->Height;
		} else {
			$defaults = Config::inst()->get($this->class, 'defaults', Config::UNINHERITED);
			return isset($defaults['Height']) ? $defaults['Height'] : self::$defaults['Height'];
		}
	}

}
