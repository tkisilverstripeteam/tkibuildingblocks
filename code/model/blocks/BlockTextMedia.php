<?php

/**
 * @Todo: Add field for menu, either JSON or menu set using a menu manager (eg. heyday/silverstripe-menumanager)
 * Check if publishable..
 */
class BlockTextMedia extends BlockBase {

	/**
	 * Different views available for user selection.
	 *
	 * @var array
	 * @config
	 */
	private static $views = array(
		'BBLayoutTextMediaNone',
		'BBLayoutTextMediaAbove',
		'BBLayoutTextMediaBelow',
		'BBLayoutTextMediaLeft',
		'BBLayoutTextMediaLeftWrap',
		'BBLayoutTextMediaRight',
		'BBLayoutTextMediaRightWrap'
	);

	/**
	 * Default view
	 * @var string
	 * @config
	 */
	private static $default_view = 'BBLayoutTextMediaNone';

	/**
	 * Different layouts available for user selection.
	 *
	 * @var array
	 * @config
	 */
	private static $media_views = array(
		'BBMediaSingleView',
		'BBMediaRandomView',
		'BBMediaMultipleVertical',
		'BBMediaMultipleHorizontal',
		'BBMediaSlideshow',
		//'BBMediaGallery',
	);

	/**
	 * Default view
	 * @var string
	 * @config
	 */
	private static $default_media_view = 'BBMediaSingleView';

	/**
	* Cache instantiated selected view class
	* @var array
	*/
	protected $selectedMediaView;

	private static $has_many = array(
		'MediaItems' => 'BBMediaItem'
	);

	private static $db = array(
		'Content' => 'HTMLText',
		'MediaView' => 'Varchar',
		'SavedMediaItems' => 'Text',
        'MediaItemAlign' => "Enum('left,center,right','center')",
        'MediaItemWidth' => 'Int',
        'MediaItemHeight' => 'Int'
	);

	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	public function singular_name()
	{
		return _t('BlockTextMedia.SINGULARNAME', 'Text/Media Block');
	}

	public function plural_name()
	{
		return _t('BlockTextMedia.PLURALNAME', 'Text/Media Blocks');
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
				'Content' => _t('BlockTextMedia.Content', 'Content'),
				'MediaItemAlign' => _t('BlockTextMedia.MediaItemAlign', 'Align media items'),
                'MediaItemWidth' => _t('BlockTextMedia.MediaItemWidth', 'Media item maximum width'),
                'MediaItemHeight' => _t('BlockTextMedia.MediaItemHeight', 'Media item maximum height')
			)
		);
	}

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->removeByName('SavedMediaItems');
     
		/*
         * Behaviour tab
         */
		$mediaViews = $this->getAvailableMediaViews();

		// If different views are available for the block, then create options
		if($mediaViews) {
			$options = array();
			$optionTpl = $this->getViewOptionTemplate();

			foreach($mediaViews as $class) {
				$view = Injector::inst()->create($class);
				// Skip invalid views
				if(!($view instanceof TkiViewInterface)) {
					continue;
				}
				// Prepare and render layout title and description
				$optionData = ArrayData::create(array(
					'Icon' => $view->getViewIcon(),
					'Title' => $view->getViewTitle(),
					'Description' => $view->getViewDescription()
				));
				$options[$class] = $optionData->renderWith($optionTpl);
			}
            
            $behaviourTab = Tab::create('Behaviour',_t('BlockTextMedia.BehaviourTab','Behaviour'));
            $fields->insertAfter('Appearance',$behaviourTab);
        
			// Options field for MediaView property
			$mediaViewField = OptionsetField::create('MediaView',_t('BlockTextMedia.MediaView','Media mode'),$options)->addExtraClass('tkimv-gridviewoptionset');
			$mediaViewField->setHasEmptyDefault(false);
            
			$fields->addFieldToTab('Root.Behaviour',$mediaViewField);
            
            /*
             *  Media view settings
             */
            
            $settingsTitle = HeaderField::create('MediaViewSettingsHeading',_t('BlockTextMedia.MediaViewSettingsHeading','Global settings'),4);
            $fields->insertAfter('MediaView',$settingsTitle);
            
			$settingsFields = array();
            
            $fields->removeByName('MediaItemAlign');
            
            // MediaItemAlign
            $alignOptions = $this->dbObject('MediaItemAlign')->enumValues();
            foreach($alignOptions as $k => $v) {
                $alignOptions[$k] = _t('BlockTextMedia.MediaItemAlign-'. $k);
            }
            $settingsFields[] = DropdownField::create('MediaItemAlign',_t('BlockTextMedia.MediaItemAlign','Align media items'),$alignOptions);
            
            // Media Width / Height
            foreach(array('MediaItemWidth','MediaItemHeight') as $column) {
                $field = $fields->dataFieldByName($column);
                $fields->removeByName($column);
                if($field) {
                    $settingsFields[] = $field;
                }
            }
            $fields->insertAfter('MediaViewSettingsHeading',CompositeField::create($settingsFields));
		}

		/*
		 * Media items tab
		 */
		$fields->findOrMakeTab('Root.MediaItems')->setTitle(_t('BlockTextMedia.MediaTab','Media'));
		
		if(!$this->exists()) {
			$fields->addFieldToTab('Root.MediaItems', LiteralField::create('ItemsAvailableAfterSaving',
				'<p class="message warning">'.
				_t('BlockTextMedia.MediaItemsAvailableAfterSaving', 'Block must be saved first before adding items')
				.'</p>'));
		} else {

			$mediaItemsGrid = $fields->dataFieldByName('MediaItems');

			// Remove relation link autocompleter
			$autoCompleter = $mediaItemsGrid->getConfig()->getComponentByType('GridFieldAddExistingAutocompleter');
			$mediaItemsGrid->getConfig()->removeComponent($autoCompleter);

			// Sorting
            if (class_exists('GridFieldOrderableRows')) {
                $mediaItemsGrid->getConfig()->addComponent(new GridFieldOrderableRows('Sort'));
            } elseif (class_exists('GridFieldSortableRows')) {
                $mediaItemsGrid->getConfig()->addComponent(new GridFieldSortableRows('Sort'));
            }

		}

		return $fields;
	}

	public function getCMSValidator() {
		return RequiredFields::create('ManyMany[BlockArea]','ViewClass');
	}
    
	public function onBeforeWrite()
	{
		parent::onBeforeWrite();
		// Simple versioning of media items relation
		$mediaItems =  $this->MediaItems();
		$mediaItemIDs = ($mediaItems instanceof DataList) ? $mediaItems->sort('Sort')->map('ID')->keys() : array();
		$this->SavedMediaItems = implode(',',$mediaItemIDs);
	}

	/**
	 * @todo Garbage cleanup routine for unlinked media items
	 */
	public function onAfterWrite()
	{
		parent::onAfterWrite();
	}

	public function onAfterDelete()
    {
        parent::onAfterDelete();
		// Clean up associated media items
        if (Versioned::current_stage() == 'Stage') {
            foreach($this->MediaItems() as $item) {
				$item->delete();
			}
        }
    }

	/*
	 * -------------------------------------------------------------------------
	 * View methods
	 * -------------------------------------------------------------------------
	 */

	/**
	 *
	 * @return array
	 */
	public function getAvailableMediaViews()
	{
		return Config::inst()->get(get_class($this),'media_views',Config::UNINHERITED);
	}

	/**
	 *
	 * @return string
	 */
	public function getDefaultMediaView()
	{
		return Config::inst()->get(get_class($this),'default_media_view',Config::UNINHERITED);
	}

	protected function getSavedMediaItems()
	{
		// Get data using IDs saved in SavedMediaItems field
		$ids = explode(',',trim($this->getField('SavedMediaItems')));
		$data = ArrayList::create(BBMediaItem::get()->byIDs($ids)->toArray());
		$sorted = ArrayList::create();
		// Sort according to order in SavedMediaItems field (versioned)
		foreach($ids as $id) {
			$obj = $data->byID($id);
			if($obj) {
				$sorted->push($obj);
			}
		}
        // Exclude disabled items
		return $sorted->exclude(array('Disabled' => 1));
	}
	
	public function setViewData($view)
	{
		// Set view dimensions - needed for accurate image resizing, etc.
		$view->width = $this->getMaxWidth();
		$view->height = $this->getMaxHeight();
	}
	
	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
	

	public function MediaView() {
		if(!empty($this->selectedMediaView)) {
			return $this->selectedMediaView;
		}
		$view = ($this->MediaView && class_exists($this->MediaView))
				? Injector::inst()->create($this->MediaView) : null;
		if($view) {
			$view->setFailover($this);
			$layoutView = $this->getSelectedView();
			$view->items = $this->getSavedMediaItems();
			$view->width = $layoutView->mediaViewWidth();
			$view->height = $layoutView->mediaViewHeight();
			$view->prepare();
		}

		return $this->selectedMediaView = $view;
	}
    

}
