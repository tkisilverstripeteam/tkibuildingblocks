<?php


class BlockNavigation extends BlockBase {

	/**
	 * Orientations available for rendering navigation
	 * @var array
	 * @config
	 */
	private static $orientations = array(
		'Horizontal',
		'Vertical'
	);

	/**
	 * Default orientation
	 * @var array
	 * @config
	 */
	private static $default_orientation = 'Horizontal';

	/**
	 * Formats available for rendering navigation
	 * @var array
	 * @config
	 */
	private static $formats = array(
		'List',
		'Menu',
		//'Buttons',  @todo
		//'Tabs', @todo
		//'Pills'
	);

	/**
	 * Default format
	 * @var array
	 * @config
	 */
	private static $default_format = 'List';


	private static $db = array(
		'Orientation' => "Varchar(20)",
		'Format' => "Varchar(20)"
	);

	private static $has_many = array(
		'NavItems' => 'BlockNavigationItem'
	);

	public function singular_name()
	{
		return _t('BlockNavigation.SINGULARNAME', 'Navigation Block');
	}

	public function plural_name()
	{
		return _t('BlockNavigation.PLURALNAME', 'Navigation Blocks');
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
				'Orientation' => _t('BlockNavigation.Orientation', 'Orientation'),
				'Format' => _t('BlockNavigation.Format', 'Format')
			)
		);
	}

	protected function templateName() {
		$class = get_class($this);
		$nameFilter = FileNameFilter::create();
		$format = (!empty($this->Format)) ? $nameFilter->filter($this->Format) : null;
		return (!empty($format)) ? $class .'_' . $format : $class;
	}

	public function forTemplate() {

		$tplName = $this->templateName();
		$tpl = SSViewer::getTemplateFileByType($tplName,'Includes');
		// Check template
		if(empty($tpl)) {
			// Fall back - to avoid fatal error
			$tpl = SSViewer::getTemplateFileByType(get_class($this),'Includes');
			// Log error
			SS_Log::log("Template $tplName not found",SS_Log::ERR);
		}
		return $this->renderWith($tpl);
	}

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		$fields->removeFieldFromTab('Root.Main','ClassName');
		$fields->removeByName('NavItems');
		$fields->removeByName('TitleTag');

		// Orientation
		$orientationOptions = $this->config()->get('orientations');
		$translatedOrientations = array();
		foreach($orientationOptions as $orientation) {
			$translatedOrientations[$orientation] = _t('BlockNavigation.Orientation-'.$orientation, $orientation);
		}
		$fields->addFieldToTab('Root.Main',
			DropdownField::create('Orientation',_t('BlockNavigation.Orientation','Orientation'),$translatedOrientations));

		// Format
		$formatOptions = (array) $this->config()->get('formats');
		$translatedFormats = array();
		foreach($formatOptions as $format) {
			$translatedFormats[$format] = _t('BlockNavigation.Format-'. $format, $format);
		}

		$fields->addFieldToTab('Root.Main',
			DropdownField::create('Format',_t('BlockNavigation.Format','Format'),$translatedFormats));

		// NavItems
		$config = GridFieldConfig_RecordEditor::create();
		$fields->addFieldToTab('Root.Main',GridField::create('NavItems',
			_t('BlockNavigationItem.PLURALNAME', 'Navigation Items'),
			$this->NavItems(),
			($this->exists() ? $config : null)));

		$config->addComponent(new GridFieldOrderableRows());

		if(!$this->exists()) {
			$fields->addFieldToTab('Root.Main', LiteralField::create('ItemsAvailableAfterSaving',
				'<p class="message warning">'.
				_t('BlockNavigationItem.ItemsAvailableAfterSaving', 'Block must be saved first before adding items')
				.'</p>')
			);
		}

		$this->extend('updateCMSFields', $fields);

		return $fields;
	}

	public function requireDefaultRecords()
    {
        parent::requireDefaultRecords();

        foreach ($this->getDefaultNavBlockNames() as $name) {
            $existing = BlockNavigation::get()->filter('Name', $name)->first();

            if (!$existing) {
                $block = BlockNavigation::create();
                $block->Name = $name;
				$block->StartID = 0;
				$block->Levels = 1;
                $block->write();

                DB::alteration_message("Navigation block '$name' created", 'created');
            }
        }
    }

	protected function getDefaultNavBlockNames() {
		return $this->config()->get('default_blocks') ?: array();
	}


	public function ShowChildren() {
		return ($this->SubLevels > 0 && $this->NavItems()->count());
	}
}
