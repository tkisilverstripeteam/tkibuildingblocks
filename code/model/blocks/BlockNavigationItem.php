<?php

class BlockNavigationItem extends DataObject 
{
	private static $db = array(
		'Type' => "Enum('internal,external')",
		'SubLevels' => 'Int',
		'Label' => 'Varchar(255)',
		'URL' => 'Text',
		'Sort' => 'Int',
		'Target' => "Enum('_self,_blank','_self')"
	);

	private static $has_one = array(
		'Page' => 'SiteTree',
		'NavBlock' => 'BlockNavigation'
	);
	
	private static $summary_fields = array(
		'MenuTitle' => 'Title',
		'Link' => 'Link',
		'Type' => 'Type',
		'Target' => 'Target window'
	);
	
	private static $defaults = array(
		'Type' => 'internal',
		'URL' => 'http://',
		'Target' => '_self'
	);
	
	/**
     * @var string
     */
    private static $default_sort = 'Sort';
	
	public function singular_name()
	{
		return _t('BlockNavigationItem.SINGULARNAME', 'Navigation Item');
	}

	public function plural_name()
	{
		return _t('BlockNavigationItem.PLURALNAME', 'Navigation Items');
	}

	public function MenuTitle() {
		switch($this->Type) {
			case 'internal':
				$page = $this->Page();
				return ($page) ? $page->MenuTitle : '';
				break;
			case 'external':
				return $this->Label;
				break;
		}
	}
	
	public function Children() {
		if($this->Type === 'external' || $this->SubLevels === 0) {
			return false;
		}
		$page = $this->Page();
		return ($page) ? $page->Children()->filter('ShowInMenus',1) : false;
	}
	
	public function Link()
	{
		switch($this->Type) {
			case 'internal':
				$page = $this->Page();
				return ($page) ? $page->Link() : '';
				break;
			case 'external':
				return $this->URL;
				break;
		}
		
	}
	
	public function AbsoluteLink()
	{
		switch($this->Type) {
			case 'internal':
				$page = $this->Page();
				return ($page) ? $page->AbsoluteLink() : '';
				break;
			case 'external':
				return $this->URL;
				break;
		}
		
	}
	
	public function isCurrent()
	{
		return ($this->PageID) ? $this->PageID == Director::get_current_page()->ID : false;
	}
	
	public function isSection() {
		if(!$this->PageID) {
			return false;
		} else {
			return $this->isCurrent() || (
				Director::get_current_page() instanceof SiteTree && in_array($this->PageID, Director::get_current_page()->getAncestors()->column())
			);
		}
	}
	
	public function getCMSFields()
	{
		// Type options
		$typeOptions = $this->dbObject('Type')->enumValues();
		foreach($typeOptions as $k => $v) {
			$typeOptions[$k] = _t('HtmlEditorField.LINK'.strtoupper($k));
		}
		
		// Translate Target Enum values
		$targetOptions = $this->dbObject('Target')->enumValues();
		foreach($targetOptions as $k => $v) {
			$targetOptions[$k] = _t('BlockNavigationItem.Target'. $k);
		}
		
		$treeField = TreeDropdownField::create('PageID',_t('BlockNavigationItem.Page','Page'),'SiteTree');
		$levelOptions = array_combine(range(0,3),range(0,3));
		$fields = FieldList::create(array(
			$typeField = OptionsetField::create('Type',
				_t('BlockNavigationItem.Type','Type'),
				$typeOptions,
				'internal'
			),
			$treeFieldWrapper = DisplayLogicWrapper::create($treeField),
			$levelsField = DropdownField::create('SubLevels',_t('BlockNavigationItem.SubLevels','Child levels'),$levelOptions,0),
			$labelField = TextField::create('Label',_t('BlockNavigationItem.Label','Label')),
			$linkField = TextField::create('URL',_t('BlockNavigationItem.URL','Link URL')),
			$targetField = OptionsetField::create('Target',_t('BlockNavigationItem.Target','Target window'),$targetOptions)
		));
		
		// Display logic
		$treeField->displayUnless('Type')->isEqualTo('external');
		$labelField->hideUnless('Type')->isEqualTo('external');
		$linkField->hideUnless('Type')->isEqualTo('external');
		
		// Extension hook
		$this->extend('updateCMSFields', $fields);
		
		return $fields;
	}
	
	
	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
				'Content' => _t('BlockNavigation.Links', 'Links'),
			)
		);
	}
	
	
	protected function onBeforeWrite() {
		parent::onBeforeWrite();
		
		switch($this->Type) {
			case 'internal':
				$this->setField('URL',null);
				$this->setField('Label',null);
				break;
			case 'external':
				$this->setField('PageID',0);
				break;
		}
		
		
	}
	
}