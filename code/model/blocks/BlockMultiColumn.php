<?php

/**
 * @Todo:
 */
class BlockMultiColumn extends BlockBase {

    /**
	 * Different views available for user selection.
	 *
	 * @var array
	 * @config
	 */
	private static $views = array(
		'BBLayoutMultiColumn'
	);
    
	/**
	 * Default view
	 * @var string
	 * @config
	 */
	private static $default_view = 'BBLayoutMultiColumn';

    private static $hide_views = true;
    
	private static $db = array(
        'Columns'  => 'Int',
		'Content1' => 'HTMLText',
        'Content2' => 'HTMLText',
        'Content3' => 'HTMLText',
        'Content4' => 'HTMLText'
	);

    protected $cached = [];
    
	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	public function singular_name()
	{
		return _t('BlockMultiColumn.SINGULARNAME', 'Multi-column Block');
	}

	public function plural_name()
	{
		return _t('BlockMultiColumn.PLURALNAME', 'Multi-column Blocks');
	}

	public function getCMSFields()
	{
        Requirements::css(TKIBUILDINGBLOCKS_DIR . '/css/tkibuildingblocks-admin.css');
		$fields = parent::getCMSFields();
        
        $fields->removeByName('Columns');
		$fields->removeByName('Content1');
        $fields->removeByName('Content2');
        $fields->removeByName('Content3');
        $fields->removeByName('Content4');
        
        // Columns
        $contentTab = Tab::create('Content')->setTitle(_t('BlockMultiColumn.Content','Content'));
        $fields->insertAfter('Main',$contentTab);
        $columnsField = DropdownField::create('Columns',_t('BlockMultiColumn.Columns','Columns'),[
            2 => _t('BlockMultiColumn.Columns-2','2 Columns'),
            3 => _t('BlockMultiColumn.Columns-3','3 Columns'),
            4 => _t('BlockMultiColumn.Columns-4','4 Columns'),
        ],2);
        $fields->addFieldToTab('Root.Content',$columnsField);
        
        // Column 1
        //$column1Tab = Tab::create('Column1')->setTitle(_t('BlockMultiColumn.Column1Tab','Column 1'));
        //$fields->push($column1Tab);
        
        $content1 = HtmlEditorField::create('Content1',_t('BlockMultiColumn.Column1','Column #1'));
        $fields->addFieldToTab('Root.Content',$content1);
        
        // Column 2
        //$column2Tab = DisplayLogicWrapper::create(Tab::create('Column2')->setTitle(_t('BlockMultiColumn.Column2Tab','Column 2')));
        //$fields->push($column2Tab);
        
        $content2 = HtmlEditorField::create('Content2',_t('BlockMultiColumn.Content2','Content #2'));
        $fields->addFieldToTab('Root.Content',$content2);
       
        // Column 3
        //$column3Tab = DisplayLogicWrapper::create(Tab::create('Column3')->setTitle(_t('BlockMultiColumn.Column3Tab','Column 3')));
        //$fields->push($column3Tab);
        
        $content3 = HtmlEditorField::create('Content3',_t('BlockMultiColumn.Content3','Content #3'));
        $fields->addFieldToTab('Root.Content',$content3);
        
        // Column 4
        //$column4Tab = DisplayLogicWrapper::create(Tab::create('Column4')->setTitle(_t('BlockMultiColumn.Column4Tab','Column 4')));
        //$fields->push($column4Tab);
        
        $content4 = HtmlEditorField::create('Content4',_t('BlockMultiColumn.Content4','Content #4'));
        $fields->addFieldToTab('Root.Content',$content4);
        
        // Display logic
		$content3->displayIf('Columns')->isGreaterThan(2);
        $content4->displayIf('Columns')->isGreaterThan(3);
        
		return $fields;
	}

	public function getCMSValidator() {
		return RequiredFields::create('ManyMany[BlockArea]','ViewClass');
	}

    public function onBeforeWrite()
	{
		parent::onBeforeWrite();
        
        $viewClass = Config::inst()->get(get_class($this),'default_view',Config::UNINHERITED) ?: static::$default_view;
		if($viewClass && $this->ViewClass !== $viewClass) {
            $this->ViewClass = $viewClass;
        }
	}
    
	/*
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */

	

	/*
	 * -------------------------------------------------------------------------
	 * View methods
	 * -------------------------------------------------------------------------
	 */

    public function getGridColumns()
	{
        if(!isset($this->cached['GridColumns'])) {
            $this->cached['GridColumns'] = Config::inst()->get(get_class($this),'grid_columns',Config::UNINHERITED) ?: 12;
        }
		return $this->cached['GridColumns'];
	}
    
    public function defaultColumnFactor()
	{
        if(!isset($this->cached['defaultColumnFactor'])) {
            $gridColumns = $this->getGridColumns();
            $this->cached['defaultColumnFactor'] = ($this->Columns) ? ceil(intval($gridColumns) / intval($this->Columns)) : $gridColumns;
        }
		return $this->cached['defaultColumnFactor'];
	}
    
    public function ColumnData() 
    {
        if(!isset($this->cached['ColumnData'])) {
            $arrList = new ArrayList();  

            for($i=1; $i <= intval($this->Columns); $i++) {
                $contentField = 'Content'.$i;
                $data = [
                    'Index' => $i,
                    'First' => ($i === 1),
                    'Last' => ($i === intval($this->Columns)),
                    'Content' => $this->dbObject($contentField),
                    'RelativeWidth' => $this->defaultColumnFactor() // @todo user settable per column?
                ];
                $arrList->push($data);
            }
            
            $this->cached['ColumnData'] = $arrList;
        }
        return $this->cached['ColumnData'];
    }
    
	public function setViewData($view)
	{
        
        
	}

}
