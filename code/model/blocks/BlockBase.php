<?php


class BlockBase extends Block
{

	/**
	 * Default block area
	 * @var string
	 * @config
	 */
	private static $default_block_area;

	/**
	 * Instance cache for block area config
	 * @var array|boolean
	 */
	protected $blockAreaConfig;

	/**
	 * Default max width
	 * @var string
	 * @config
	 */
	private static $max_width = 800;

	/**
	 * Default max height
	 * @var string
	 * @config
	 */
	private static $max_height = 800;

	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	protected function getCMSRequiredFields()
	{
		return array('ManyMany[BlockArea]');
	}

	public function getCMSValidator() {
		return RequiredFields::create($this->getCMSRequiredFields());
	}

	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */

     /**
	 * Renders template
	 * @return HTMLText
	 */
	public function xforTemplate()
	{
		$view = $this->getSelectedView();
        if($view) {
			return $view->renderWith($view->viewTemplates());
        } elseif ($this->BlockArea) {
            $templates = array(
                $this->class.'_'.$this->BlockArea,
                $this->class
            );
			return $this->renderWith($templates);
        }
 
	}
    
	/**
	 * Gets configuration for the associated block area
	 * @param string $key - Optional key to return value for
	 * @return mixed
	 */
	protected function getBlockAreaConfig($key=null)
	{
		if(!$this->blockAreaConfig) {
			$blockManager = Injector::inst()->get('BlockManager');
			$this->blockAreaConfig = ($blockManager) ? $blockManager->getAreasForTheme(null, false) : array();
		}
		if($key) {
			return (isset($this->blockAreaConfig[$this->BlockArea]) && isset($this->blockAreaConfig[$this->BlockArea][$key]))
				? $this->blockAreaConfig[$this->BlockArea][$key] : null;
		} else {
			return isset($this->blockAreaConfig[$this->BlockArea]) ? $this->blockAreaConfig[$this->BlockArea] : null;
		}
	}
	/*
	 * -------------------------------------------------------------------------
	 * View methods
	 * -------------------------------------------------------------------------
	 */
    
	public function getMaxWidth()
	{
		// First try the block area config - the most accurate.
		$width = $this->getBlockAreaConfig('max_width');
		if(is_numeric($width)) {
			return (int) $width;
		}
		// Try the configured value for this class
		else {
			return (int) Config::inst()->get(get_class($this),'max_width',Config::INHERITED);
		}
	}

	/**
	 * Gets the max height configured
	 * @return int
	 */
	public function getMaxHeight()
	{
		// First try the block area config - the most accurate.
		$height = $this->getBlockAreaConfig('max_height');

		if(is_numeric($height)) {
			return (int) $height;
		}
		// Try the configured value for this class
		else {
			return (int) Config::inst()->get(get_class($this),'max_height',Config::INHERITED);
		}
	}
}
