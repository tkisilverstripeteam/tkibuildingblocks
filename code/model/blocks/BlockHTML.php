<?php


class BlockHTML extends BlockBase implements PermissionProvider {

	private static $db = array(
		'Content' => 'HTMLText'
	);

	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	public function singular_name()
	{
		return _t("$this->class.SINGULARNAME", 'HTML Block');
	}

	public function plural_name()
	{
		return _t("$this->class.PLURALNAME", 'HTML Blocks');
	}

	public function fieldLabels($includeRelations = true)
	{
		return array_merge(
			parent::fieldLabels($includeRelations),
			array(
				'Content' => _t("$this->class.Content", 'HTML Content')
			)
		);
	}

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		$htmlField = TextareaField::create('Content',_t('BlockHTML.Content','HTML Content'));
		$htmlField->setRows(20);
		$fields->replaceField('Content',$htmlField);

		return $fields;
	}

	public function canCreate($member = null)
    {
        return Permission::check('ADMIN') || Permission::check('BLOCKHTML_MANAGE');
    }

	public function canEdit($member = null)
    {
        return Permission::check('ADMIN') || Permission::check('BLOCKHTML_MANAGE');
    }

    public function canDelete($member = null)
    {
        return Permission::check('ADMIN') || Permission::check('BLOCKHTML_MANAGE');
    }

    public function canPublish($member = null)
    {
        return Permission::check('ADMIN') || Permission::check('BLOCKHTML_MANAGE');
    }

    public function providePermissions()
    {
        return array(
            'BLOCKHTML_MANAGE' => array(
                'name' => _t('BlockHTML.ManagePermission', 'Manage an HTML Block'),
                'category' => _t('Block.PermissionCategory', 'Blocks'),
            )
        );
    }
	
}
