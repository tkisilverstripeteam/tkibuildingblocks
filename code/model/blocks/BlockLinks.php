<?php

/**
 * @Todo:
 */
class BlockLinks extends BlockBase {

	/**
	 * Different views available for user selection.
	 *
	 * @var array
	 * @config
	 */
	private static $views = array(
		'BBLinksListView',
		'BBLinksMediaView',
		'BBLinksGridView',
        'BBLinksButtonGridView'
	);

	/**
	 * Default view
	 * @var string
	 * @config
	 */
	private static $default_view = 'BBLinksListView';

	private static $db = array(
		'SavedLinks' => 'Text'
	);

	private static $has_many = array(
		'Links' => 'BBLink'
	);

	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */

	public function singular_name()
	{
		return _t('BlockLinks.SINGULARNAME', 'File Links Block');
	}

	public function plural_name()
	{
		return _t('BlockLinks.PLURALNAME', 'File Links Blocks');
	}

	public function getCMSFields()
	{
        Requirements::css(TKIBUILDINGBLOCKS_DIR . '/css/tkibuildingblocks-admin.css');
		$fields = parent::getCMSFields();
		$fields->removeByName('SavedLinks');
		/*
		 * File links tab
		 */

		$fields->findOrMakeTab('Root.Links')->setTitle(_t('BlockLinks.LinksTab', 'Links'));
		if(!$this->exists()) {
			$fields->addFieldToTab('Root.Links', LiteralField::create('ItemsAvailableAfterSaving',
				'<p class="message warning">'.
				_t('BlockLinks.LinksAvailableAfterSaving', 'Block must be saved first before adding items')
				.'</p>'));
		} else {

			$linksGrid = $fields->dataFieldByName('Links');
            
			// Remove relation link autocompleter
			$autoCompleter = $linksGrid->getConfig()->getComponentByType('GridFieldAddExistingAutocompleter');
			//$linksGrid->getConfig()->removeComponent($autoCompleter);

			// Sorting
            if (class_exists('GridFieldOrderableRows')) {
                $linksGrid->getConfig()->addComponent(new GridFieldOrderableRows('Sort'));
            } elseif (class_exists('GridFieldSortableRows')) {
                $linksGrid->getConfig()->addComponent(new GridFieldSortableRows('Sort'));
            }
            
            // Grid columns
            $summaryFields = [
                'ID' => array('title' => _t('Block.ID', 'ID'), 'field' => 'LiteralField'),
                'Title' => array('title' => _t('Block.Title', 'Title'), 'field' => 'LiteralField'),
                'FormattedSource' => array('title' => _t('BBLink.Source', 'Source'), 'field' => 'LiteralField'),
                'Type' => array('title' => _t('BBLink.Type', 'Type'), 'field' => 'LiteralField'),
                'DisabledStatus' => array('title' => _t('BBLink.DisabledStatus', 'Status'), 'field' => 'HTMLText')
            ];
            
            $dataColumns = $linksGrid->getConfig()->getComponentByType('GridFieldDataColumns');
            if($dataColumns) {
                $dataColumns->setDisplayFields($summaryFields);
            }
		}
        

		return $fields;
	}

	public function getCMSValidator() {
		return RequiredFields::create('ManyMany[BlockArea]','ViewClass');
	}

	public function onBeforeWrite()
	{
		parent::onBeforeWrite();
		// Simple versioning of media items relation
		$links =  $this->Links();
		$linkIDs = ($links instanceof DataList) ? $links->sort('Sort')->map('ID')->keys() : array();
		$this->SavedLinks = implode(',',$linkIDs);
	}

	/**
	 * @todo Garbage cleanup routine for unlinked media items
	 */
	public function onAfterWrite()
	{
		parent::onAfterWrite();
	}

	public function onAfterDelete()
    {
        parent::onAfterDelete();
		// Clean up associated media items
        if (Versioned::current_stage() == 'Stage') {
            foreach($this->Links() as $item) {
				$item->delete();
			}
        }
    }

	/*
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */

	public function getSavedLinks()
	{
		// Get data using IDs saved in SavedLinks field
		$ids = explode(',',trim($this->getField('SavedLinks')));
		$data = ArrayList::create(BBLink::get()->byIDs($ids)->toArray());
		$sorted = ArrayList::create();
		// Sort according to order in SavedLinks field (versioned)
		foreach($ids as $id) {
			$obj = $data->byID($id);
			if($obj) {
				$sorted->push($obj);
			}
		}
        // Exclude disabled items
		return $sorted->exclude(array('Disabled' => 1));
	}

	/*
	 * -------------------------------------------------------------------------
	 * View methods
	 * -------------------------------------------------------------------------
	 */

	public function setViewData($view)
	{
		$view->items = $this->getSavedLinks();
	}

}
