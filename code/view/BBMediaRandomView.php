<?php

class BBMediaRandomView extends BBMediaSingleView
{
	
	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-mode-random.png';
	
	protected function prepareItems($items)
	{
		$offset = rand(0,$items->count() - 1);
		$items = ArrayList::create(array($items->offsetGet($offset)));
		foreach($items as $item) {
			if(is_object($item)) {
				$this->setItemProperties($item);
			}
		}
		return $items;
		
	}

}
