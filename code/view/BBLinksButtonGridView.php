<?php

class BBLinksButtonGridView extends BBLinksMediaView
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-links-grid.png';


	/**
	 * @config
	 * @var string
	 * @todo

	private static $default_thumbnail = '';
	*/

	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_width = 100;

	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_height = 100;

    
    protected function prepareMediaElement($item)
	{
		$width = $this->getThumbnailWidth();
		$height = $this->getThumbnailHeight();

		if($item && $item->ThumbnailID) {
			if($width && $height) {
				return $item->Thumbnail()->Fill($width,$height);
			} else {
				return $item->Thumbnail();
			}
		}
		else {
			return '';
		}
	}

}
