<?php

abstract class BBMediaView extends TkiView
{
	
    /**
	 * Container padding in pixels
	 * @config
	 * @var int
	 */
	private static $padding = 0;

	/**
	 * Media margin in pixels
	 * @config
	 * @var int
	 */
	private static $itemMargin = 10;

	/**
	 * Media padding in pixels
	 * @config
	 * @var int
	 */
	private static $itemPadding = 0;

	/**
	 * Media border in pixels
	 * @config
	 * @var int
	 */
	private static $mediaBorder = 0;

		
	/**
	 * Maximum width of view in pixels
	 * @var int
	 */
	public $width;
	
	/**
	 * Maximum height of view in pixels
	 * @var int
	 */
	public $height;
	
	
	/**
	 * Media items - set by parent object
	 * @var ArrayList
	 */
	public $items;
	
    public function ViewStyles()
    {
        $ViewClass = $this->ViewClass();
        $styles = ".bb-textmedia__items.$ViewClass {padding:". $this->Padding() ."px;} ";
        $styles .= ".$ViewClass .bb-textmedia__itemwrap {margin-right:". $this->ItemMargin() ."px;}";
        $styles .= ".$ViewClass .bb-textmedia__item {padding:". $this->ItemPadding() ."px;} ";
        $styles .= ".$ViewClass .bb-textmedia__item img {border-width: ". $this->MediaBorder() . "px;} ";
        return $styles;
    }
    
	public function ViewItems()
	{
		if(!($this->items instanceof SS_List)) {
			return ArrayList::create(array());
		}
		return $this->prepareItems($this->items);
	}

	public function MediaWidth()
	{
       return !empty($this->MediaItemWidth) ? $this->MediaItemWidth : (int) $this->width;
	}
	
	public function MediaHeight()
	{
		return !empty($this->MediaItemHeight) ? $this->MediaItemHeight : (int) $this->height;
	}
	
    
    public function Padding()
    {
        return (int) $this->config()->get('padding');
    }
    
    public function ItemPadding()
    {
        return (int) $this->config()->get('itemPadding');
    }
    
    public function ItemMargin()
    {
        return (int) $this->config()->get('itemMargin');
    }
    
    public function MediaBorder()
    {
        return (int) $this->config()->get('mediaBorder');
    }
    
	public function ItemWidth($imageWidth)
	{
		$mediaWidth = (int) ($imageWidth ?: $this->MediaWidth());
		return (int) ($mediaWidth + ($this->ItemPadding() * 2) + ($this->MediaBorder() * 2));
	}
            
	protected function prepareMediaElement($item)
	{
		if($item && $item->InternalImageID) {
			$image = $item->InternalImage()->FitMax($this->MediaWidth(),$this->MediaHeight());
            // Override image title, if Alt or Title supplied
            $alt = $item->Alt ?: $item->Title;
            if(!empty($alt)) {
               $image->setField('Title',$alt); 
            }
            return $image;
		}
		/** @todo Embedded object 
		elseif($item->SourceURL) {
			return '';
		}*/
		else {
			return '';
		} 
	}
	
	/**
	 * Prepare / modify list and individual items
	 * @param DataList $items
	 * @return ArrayList
	 */
	protected function prepareItems($items)
	{
		foreach($items as $item) {
			if(is_object($item)) {
				$this->setItemProperties($item);
			}
		}
		return $items;
	}
	
	protected function setItemProperties($item)
	{
		$item->MediaElement = $this->prepareMediaElement($item);
		$item->ItemWidth = $this->ItemWidth(($item->MediaElement) ? $item->MediaElement->Width : 0);
	}
}
