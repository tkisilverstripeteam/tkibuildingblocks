<?php

class BBLayoutTextMediaRight extends BBLayoutTextMedia
{

	/**
	 * @config
	 * @var string
	 */
	
	private static $icon = 'tkibuildingblocks/images/icons/icon-layout-textmedia-right.png';

	
	function mediaViewWidth()
	{
		return round(intval($this->width) / 2);
	}
}
