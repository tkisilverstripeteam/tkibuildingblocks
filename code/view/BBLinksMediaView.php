<?php

class BBLinksMediaView extends TkiView
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-links-media.png';

	/**
	 * @config
	 * @var string
	 * @todo
	 */
	private static $default_thumbnail = 'tkibuildingblocks/images/thumbs/thumb-links-default.png';


	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_width = 64;

	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_height = 64;


	/**
	 * Instance cache
	 * @var Image
	 * @todo
	protected $defaultThumbnail;
	*/
	/**
	 * Instance cache
	 * @var int
	 */
	protected $thumbWidth;

	/**
	 * Instance cache
	 * @var int
	 */
	protected $thumbHeight;

	public $items;

	public function ViewItems()
	{
		if(!($this->items instanceof SS_List)) {
			return ArrayList::create(array());
		}
		return $this->prepareItems($this->items);
	}

	/**
	 * Prepare / modify list and individual items
	 * @param DataList $items
	 * @return ArrayList
	 */
	protected function prepareItems($items)
	{
		foreach($items as $item) {
			if(is_object($item)) {
				$item->MediaElement = $this->prepareMediaElement($item);
			}
		}
		return $items;
	}

	/**
	 * Generates thumbnail tag.
	 * If both width and height are configured, the thumbnail will be cropped
	 * to configured dimensions.
	 * @param BBLink $item
	 * @return string
	 */
	protected function prepareMediaElement($item)
	{
		$width = $this->getThumbnailWidth();
		$height = $this->getThumbnailHeight();

		if($item && $item->ThumbnailID) {
			if($width && $height) {
				return $item->Thumbnail()->Fill($width,$height);
			} else {
				return $item->Thumbnail();
			}
		}
		else {
			$styles = '';
			if($width) {
				$styles .= "width: {$width}px; ";
			}
			if($height) {
				$styles .= "height: {$height}px; ";
			}
			return '<img class="bb-link__defaultthumb" style="'. $styles .'" src="'. $this->DefaultThumbnailURL() .'" alt="'. $item->Title .'" />';
		}
	}

	/**
	 *
	 * @todo
	 */
	public function DefaultThumbnailURL()
	{
		return Config::inst()->get(get_class($this),'default_thumbnail');
	}

	protected function getThumbnailWidth()
	{
		if(empty($this->thumbWidth)) {
			$this->thumbWidth = (int) Config::inst()->get(get_class($this),'thumbnail_width');
		}
		return $this->thumbWidth;
	}

	protected function getThumbnailHeight()
	{
		if(empty($this->thumbHeight)) {
			$this->thumbHeight = (int) Config::inst()->get(get_class($this),'thumbnail_height');
		}
		return $this->thumbHeight;
	}
}
