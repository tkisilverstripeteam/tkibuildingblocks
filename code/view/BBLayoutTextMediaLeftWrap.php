<?php

class BBLayoutTextMediaLeftWrap extends BBLayoutTextMedia
{

	/**
	 * @config
	 * @var string
	 */
	
	private static $icon = 'tkibuildingblocks/images/icons/icon-layout-textmedia-leftwrap.png';
	
	function mediaViewWidth()
	{
		return round(intval($this->width) / 2);
	}
}
