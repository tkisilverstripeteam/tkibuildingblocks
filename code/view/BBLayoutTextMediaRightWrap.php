<?php

class BBLayoutTextMediaRightWrap extends BBLayoutTextMedia
{

	/**
	 * @config
	 * @var string
	 */
	
	private static $icon = 'tkibuildingblocks/images/icons/icon-layout-textmedia-rightwrap.png';

	
	function mediaViewWidth()
	{
		return round(intval($this->width) / 2);
	}
	
}
