<?php

class BBLinksGridView extends BBLinksMediaView
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-links-grid.png';


	/**
	 * @config
	 * @var string
	 * @todo

	private static $default_thumbnail = '';
	*/

	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_width = 100;

	/**
	 * @config
	 * @var int
	 */
	private static $thumbnail_height = 100;


}
