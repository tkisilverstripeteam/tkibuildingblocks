<?php

class BBLinksListView extends TkiView
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-links-list.png';

	public $items;

	public function ViewItems()
	{
		if(!($this->items instanceof SS_List)) {
			return ArrayList::create(array());
		}
		return $this->prepareItems($this->items);
	}

	/**
	 * Prepare / modify list and individual items
	 * @param DataList $items
	 * @return ArrayList
	 */
	protected function prepareItems($items)
	{
		foreach($items as $item) {
			if(is_object($item)) {
				$item->IconClass = $this->determineIconClass($item);
			}
		}
		return $items;
	}

	protected function determineIconClass($item)
	{
		$type = (!empty($item->Type)) ? $item->Type : '';
		switch($type) {
			case 'audio':
				return 'file-audio-o';
				break;
			case 'mov':
				return 'file-movie-o';
				break;
			case 'zip':
				return 'file-archive-o';
				break;
			case 'image':
				return 'file-image-o';
				break;
			case 'doc':
				return 'file-text-o';
				break;
			case 'webpage':
				return ($item->Source === 'external') ? 'external-link' : 'link';
				break;
			case 'spreadsheet':
				return 'file-excel-o';
				break;
			case 'pdf':
				return 'file-pdf-o';
				break;
			default:
				return 'file-o';
				break;
		}


	}


}
