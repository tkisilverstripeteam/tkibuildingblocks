<?php

/**
 * @todo make some options available in CMS behaviour tab
 */
abstract class AbstractBBMediaSlideshow extends BBMediaView
{

	/**
	 * Determines maximum width for media item. 
     * Uses MediaItemWidth as maximum width, if set. Otherwise it 
     * uses the view width.
	 * @return int
	 */
	public function MediaWidth()
	{
        // Check instance cache
		if($this->mediaWidth) {
			return $this->mediaWidth;
		}
        // Check explicitly set on parent
        if(!empty($this->MediaItemWidth)) {
            $this->mediaWidth = (int) $this->MediaItemWidth;
        } 
        // Use view width as max width
        else {
            $this->mediaWidth = (int) $this->width;
        }
        
		return $this->mediaWidth;
	}

    /**
	 * Determines maximum height for media item. 
     * Uses MediaItemHeight as maximum height, if set. Otherwise it 
     * uses the view height.
	 * @return int
	 */
	public function MediaHeight()
	{
        // Check instance cache
        if($this->mediaHeight) {
			return $this->mediaHeight;
		}
        // Check explicitly set on parent
        if(!empty($this->MediaItemHeight)) {
            $this->mediaHeight = (int) $this->MediaItemHeight;
        } 
        // Use view height as max height
        else {
            $this->mediaHeight = (int) $this->height;
        }
        
		return $this->mediaHeight;
	}

	/**
	 * Prepare / modify list and individual items
	 * @param DataList $items
	 * @return ArrayList
	 */
	protected function prepareItems($items)
	{
		foreach($items as $item) {
			if(is_object($item)) {
				$this->setItemProperties($item);
			}
		}
		return $items;
	}

	protected function prepareMediaElement($item)
	{
		if($item && $item->InternalImageID) {
			$image = $item->InternalImage()->FitMax($this->MediaWidth(),$this->MediaHeight());
			// Override image title, if Alt or Title supplied
            $alt = $item->Alt ?: $item->Title;
            if(!empty($alt)) {
               $image->setField('Title',$alt); 
            }
			return $image;
		}
		/** @todo Embedded object
		elseif($item->SourceURL) {
			return '';
		}*/
		else {
			return '';
		}
	}

}
