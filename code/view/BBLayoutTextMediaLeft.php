<?php

class BBLayoutTextMediaLeft extends BBLayoutTextMedia
{

	/**
	 * @config
	 * @var string
	 */
	
	private static $icon = 'tkibuildingblocks/images/icons/icon-layout-textmedia-left.png';

	function mediaViewWidth()
	{
		return round(intval($this->width) / 2);
	}

}
