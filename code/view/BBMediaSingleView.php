<?php

class BBMediaSingleView extends BBMediaView
{
	
	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-mode-single.png';

	protected function prepareItems($items)
	{
		$items = ArrayList::create(array($items->first()));
		foreach($items as $item) {
			if(is_object($item)) {
				$this->setItemProperties($item);
			}
		}
		return $items;
	}
	

	
	
}
