<?php

abstract class BBLayoutTextMedia extends TkiView
{

	/**
	 * Maximum width of view in pixels
	 * @var int
	 */
	public $width;
	
	/**
	 * Maximum height of view in pixels
	 * @var int
	 */
	public $height;
	

	function mediaViewWidth()
	{
		return $this->width;
	}
	
	function mediaViewHeight()
	{
		return $this->height;
	}
}
