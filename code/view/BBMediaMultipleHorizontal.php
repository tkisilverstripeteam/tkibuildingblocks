<?php

class BBMediaMultipleHorizontal extends BBMediaView
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-mode-multiple-horiz.png';

	// Instance cache
	protected $mediaWidth;
    
    // Instance cache
	protected $mediaHeight;
    
	/**
	 * Determines width for media item. 
     * Uses MediaItemWidth as maximum width, if set. Otherwise it calculates the 
     * media width based on item count and available space.
	 * @todo - Update to more sophisticated logic comparing image sizes and aspect ratios, etc.
	 * @return int
	 */
	public function MediaWidth()
	{
        // Check instance cache
		if($this->mediaWidth) {
			return $this->mediaWidth;
		}
        // Check explicitly set on parent
        if(!empty($this->MediaItemWidth)) {
            $this->mediaWidth = (int) $this->MediaItemWidth;
        } else {
            $this->mediaWidth = $this->calculateMediaWidth();
        }
        
		return $this->mediaWidth;
	}

	public function MediaHeight()
	{
        // Check instance cache
        if($this->mediaHeight) {
			return $this->mediaHeight;
		}
        // Check explicitly set on parent
        if(!empty($this->MediaItemHeight)) {
            $this->mediaHeight = (int) $this->MediaItemHeight;
        } 
        // Use view height as max height
        else {
            $this->mediaHeight = (int) $this->height;
        }
        
		return $this->mediaHeight;
	}
	
    protected function calculateMediaWidth()
    {
        // Count
		$count = ($this->items) ? $this->items->count() : 0;
        // Container
        $containerWidth = (int) $this->width; 
        $containerPadding = $this->Padding();

        // Margins
		$margins = $this->ItemMargin() * ($count-1);
        // Borders
		$borders = $this->MediaBorder() * $this->countItemsWithBorders();
        // Padding
		$padding = ($containerPadding + ($this->ItemPadding() * $count)) * 2;
 
        // Find available width
		$availableWidth = $containerWidth - intval($margins + $padding + $borders);
		// This can also be overrided explicitly in template, if needed
		$width = ($count) ? floor($availableWidth / $count) : $availableWidth;
        return (int) $width;
    }

    protected function countItemsWithBorders()
    {
        $count = 0;
        foreach($this->items as $item) {
            if(!$item->NoBorder) {
                ++$count;
            }
        }
        return $count;
    }
}
